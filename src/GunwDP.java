import java.io.*;
import java.net.*;
import java.util.*;


public class GunwDP {

	
	static Set<String> whitelist = new HashSet<>();
	
	
	public static void main(String[] args) throws IOException {
		
		
		
		new UDPThread().start();
		
		ServerSocket ss = new ServerSocket(6969);
		
		
		try {
			
			while(true) {
				System.out.println("Listening... " + ss.getLocalPort());
				Socket sock = ss.accept();
				
				String host = sock.getInetAddress().getHostAddress();
				
				System.out.println("TCP Connection from " + host);
				
				boolean contains = false;
				synchronized(whitelist) {
					contains = whitelist.contains(host);
				}
				
				if(contains) {
					System.out.println("accepting");
					
					try {
						Socket local = new Socket("127.0.0.1", 25565);
	
						System.out.println("local connection opened");
						new TCPThread(sock, local).start();
						new TCPThread(local, sock).start();
						
					} catch (Exception e) {
						sock.close();
					}
					
				} else {
					System.out.println("REJECTED");
					sock.close();
				}
			
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	static class TCPThread extends Thread {
		private Socket sin;
		private Socket sout;

		public TCPThread(Socket in, Socket out) {
			sin = in;
			sout = out;
		}
		
		@Override
		public void run() {
			try {
				InputStream in = sin.getInputStream();
				OutputStream out = sout.getOutputStream();
				
				byte[] gunwo = new byte[2048];
				while(true) {
					int read = in.read(gunwo);
					
					if(read == -1) {
						sout.close();
						sin.close();
						break;
					}
					
					out.write(gunwo, 0, read);
				}
			} catch (Exception e) {
				if(!sout.isClosed())
					e.printStackTrace();
			} finally {
				
				try {
	                sin.close();
                } catch (IOException e) {
	                e.printStackTrace();
                }
				
				try {
	                sout.close();
                } catch (IOException e) {
	                e.printStackTrace();
                }
			}
		}
	}
	
	static class UDPThread extends Thread {
		@Override
		public void run() {
			
			try {
	            DatagramSocket sock = new DatagramSocket(6969);
	            byte[] buf = new byte[2048];
	            DatagramPacket pack = new DatagramPacket(buf, buf.length);
				
	            while(true) {
	            	System.out.println("Listening UDP " + sock.getLocalPort());
					sock.receive(pack);
					String host = pack.getAddress().getHostAddress();
					System.out.println("Received UDP from " + host);
					
					synchronized(whitelist) {
						whitelist.add(host);
					}
				}
            } catch (Exception e) {
	            e.printStackTrace();
            	System.exit(4);
            } finally {
            }
		}
	}

}
